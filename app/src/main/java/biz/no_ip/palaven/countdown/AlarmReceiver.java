package biz.no_ip.palaven.countdown;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.text.format.DateFormat;

/**
 * Catches event's alarms and shows notification about the event's occur.
 */
public class AlarmReceiver extends BroadcastReceiver {

    public static String ARG_EVENT = "event";

    /**
     * Empty.
     */
    public AlarmReceiver() {
    }

    /**
     * Gets an event from intent and shows notification about it.
     *
     * @param context activity causing the receiver
     * @param intent  intent with event in ARG_EVENT extra
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            Event event = intent.getParcelableExtra(ARG_EVENT);
            Intent myIntent = new Intent(context, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(context, 0, myIntent, 0);
            NotificationManager nm = (NotificationManager) context.getSystemService(
                    Context.NOTIFICATION_SERVICE);
            Notification.Builder builder = new Notification.Builder(context);
            builder.setContentIntent(contentIntent)
                    .setAutoCancel(true)
                    .setTicker(context.getString(R.string.notification_ticker) + event.getName())
                    .setContentTitle(event.getName())
                    .setContentText(context.getString(R.string.notification_text) + " " +
                            DateFormat.format("HH:mm", System.currentTimeMillis()).toString())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(context.getResources(),
                            EventStyle.getIconId(event.getEventStyle())))
                    .setWhen(System.currentTimeMillis());
            Notification notification;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                notification = builder.build();
            } else {
                notification = builder.getNotification();
            }
            nm.notify(1, notification);

            Database db = new Database(context);
            event.setActive(false);
            db.saveEvent(event);
        }
    }
}
