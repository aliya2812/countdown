package biz.no_ip.palaven.countdown;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.annotation.ColorInt;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Activity with tabs. Each tab displays 1 event and counts the time remaining to it.
 * On create activity gets event list from database, initializes fragment array and ViewPager.
 * Via the SectionPagerAdapter ViewPager creates the 2 first fragments and attaches them to activity.
 * Activity saves attached fragments to array. When we leaf through the events, ViewPager creates
 * remaining fragments.
 * We can edit current event. On click the edit button opens EventActivity.
 * We can open the list of all the events. On click the list button opens EventListActivity.
 * When we return to MainActivity, it refills event fragments.
 *
 * @author Aliya Gainetdinova
 */
public class MainActivity extends AppCompatActivity {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;
    private int mEventsCount;
    private List<Event> mEvents;
    private PlaceholderFragment[] mFragments;

    public static final String ARG_EVENTID = "event_id";

    /**
     * Sets listeners to buttons and calls {@link #initEventFragments()}, {@link #initTabs()}.
     * Listener for Edit button starts EventActivity.
     * Listener for List button starts EventListActivity.
     *
     * @param savedInstanceState not used
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i(Constants.LOG_TAG, "MainActivity onCreate");

        mViewPager = (ViewPager) findViewById(R.id.viewpagerContainer);

        ImageButton btnEdit = (ImageButton) findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EventActivity.class);
                int eventNum = mViewPager.getCurrentItem();
                if (mEventsCount > eventNum) {
                    intent.putExtra(EventActivity.ARG_EVENT, mEvents.get(eventNum));
                    startActivityForResult(intent, EventActivity.EDIT_EVENT_REQUEST);
                    Log.i(Constants.LOG_TAG, "MainActivity запускаем EventActivity");
                }
            }
        });
        ImageButton btnList = (ImageButton) findViewById(R.id.btnList);
        btnList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, EventListActivity.class);
                startActivity(intent);
                Log.i(Constants.LOG_TAG, "MainActivity запускаем EventListActivity");
            }
        });

        initEventFragments();
        initTabs();

        Intent intent = getIntent();
        if (intent != null) {
            long event_id = intent.getLongExtra(ARG_EVENTID, 0);
            for (Event event : mEvents) {
                if (event.get_id() == event_id) {
                    mViewPager.setCurrentItem(mEvents.indexOf(event));
                    break;
                }
            }
        }
    }

    /**
     * Initializes variables if they are not initialized.
     */
    private void initEventFragments() {
        if (mFragments == null) {
            //вызываем этот кусок каждый раз когда нужно заполнить вкладки
            Database mDB = new Database(this);

            mEvents = mDB.getActiveEventList();
            Log.i(Constants.LOG_TAG, "получили список событий из базы");
            mEventsCount = mEvents.size();
            mFragments = new PlaceholderFragment[mEventsCount];
            Log.i(Constants.LOG_TAG, "создали массив фрагментов на " + mEventsCount + " элементов");
        } else {
            Log.i(Constants.LOG_TAG, "initEventFragments mFragments is not null");
        }
    }

    /**
     * Initializes ViewPager tabs.
     */
    private void initTabs() {

        if ((mViewPager != null) && (mSectionsPagerAdapter == null)) {
            //этот кусок должен обновлять отображение вкладок
            mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
            mViewPager.setAdapter(mSectionsPagerAdapter);
            Log.i(Constants.LOG_TAG, "привязали адаптер к вьюпейджеру");
        } else {
            Log.i(Constants.LOG_TAG, "initTabs something wrong");
        }
    }

    /**
     * Refills tabs. Calls {@link #deleteFragment(int)}} for each attached fragment,
     * then uses {@link #initEventFragments()} and {@link #initTabs()} for reinitializing tabs
     * and fills them.
     */
    //когда возвращаемся из других активити, нужно перезаполнить вкладки, потому что
    // во-первых данные вкладок могли поменяться,
    // во-вторых мог измениться список отображаемыч событий
    @Override
    protected void onRestart() {
        super.onRestart();
        //запоминаем текущую страницу чтобы вернуться на нее после перезаполнения
        int currentPosition = mViewPager.getCurrentItem();
        //сохраняем ссылки на приаттаченные фрагменты, потому что они не будут переаттачиваться
        //сам фрагмент не удаляем, только подчищаем ссылку на него в SectionPagerAdapter и
        //останавливаем таймер
        PlaceholderFragment[] tmpFragments = new PlaceholderFragment[mFragments.length];
        for (int i = 0; i < mFragments.length; i++) {
            tmpFragments[i] = mFragments[i];
            deleteFragment(i);
        }

        //костыли - нужно заполнить вкладки как будто они еще не заполнены
        mFragments = null;
        mSectionsPagerAdapter = null;
        initEventFragments();
        initTabs();

        //но поскольку повторного аттача фрагментов не было, в массиве mFragments они не прописались
        //восстанавливаем из временного массива и обновляем содержание вкладки
        for (int i = 0; i < mEventsCount; i++) {
            if (i < tmpFragments.length) {
                mFragments[i] = tmpFragments[i];
            }
            if (mFragments[i] != null) {
                mFragments[i].update(mEvents.get(i));
            }
        }

        //восстанавливаем активную страницу, если конечно она не вышла за границы активных событий
        //в противном случае перейдем на последнюю
        if (currentPosition >= mEventsCount) {
            currentPosition = mEventsCount - 1;
        }
        mViewPager.setCurrentItem(currentPosition);
        //tmpFragments = null;
    }

    /**
     * Saves the link on the new fragment for later use.
     *
     * @param fragment a new fragment
     */
    public void addFragment(PlaceholderFragment fragment) {
        int i = 0;
        //при повороте экрана аттач фрагментов-вкладок выполняется раньше чем onCreate активити
        //поэтому здесь может быть исключительная ситуация
        if (mFragments == null) {
            Log.e(Constants.LOG_TAG, "mFragments is null in addFragment");
        }
        //добавляем фрагмент в первый незанятый элемент массива
        while ((i < mFragments.length) && (mFragments[i] != null)) {
            i++;
        }
        if (i < mFragments.length) {
            mFragments[i] = fragment;
            Log.i(Constants.LOG_TAG, "добавили фрагмент " + fragment.getEventName() + " в массив");
        }
    }

    /**
     * Stops fragment's timer
     *
     * @param position number of tab that we want to clear
     */
    public void deleteFragment(int position) {
        if (mFragments[position] != null) {
            mFragments[position].stopCounter();
        }
        //здесь вообще что-нибудь происходит?
        mSectionsPagerAdapter.destroyItem(mViewPager, position,
                mSectionsPagerAdapter.getItem(position));
        //Log.i("Timer", "удаляем фрагмент "+mFragments[position].getEventName()+" из массива");
        //mFragments[position] = null;
    }

    //нужен ли еще этот метод?
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.i(Constants.LOG_TAG, "вернулись из другой активити");
        if (requestCode == EventActivity.EDIT_EVENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                mEvents.set(mViewPager.getCurrentItem(),
                        (Event) data.getParcelableExtra(EventActivity.ARG_EVENT));
                Log.i(Constants.LOG_TAG, "изменили событие " + mViewPager.getCurrentItem());
                mFragments[mViewPager.getCurrentItem()].update(
                        mEvents.get(mViewPager.getCurrentItem()));
                Log.i(Constants.LOG_TAG, "вызвали обновление фрагмента "
                        + mFragments[mViewPager.getCurrentItem()].getEventName());
            }
        }
    }

    /**
     * Starts notification and stops timers
     */
    @Override
    protected void onStop() {
        startNotification();
        for (int i = 0; i < mEventsCount; i++) {
            if (mFragments[i] != null) {
                mFragments[i].stopCounter();
            }
        }
        super.onStop();
    }

    /**
     * Stops notification and starts timers
     */
    @Override
    protected void onResume() {
        stopNotification();
        for (int i = 0; i < mEventsCount; i++) {
            if (mFragments[i] != null) {
                mFragments[i].startCounter();
            }
        }
        super.onResume();
    }

    /**
     * For each active event creates alarm on it's onset time.
     */
    private void startNotification() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        for (Event event : mEvents) {
            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
            intent.putExtra(AlarmReceiver.ARG_EVENT, event);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),
                    (int) (event.get_id() % Integer.MAX_VALUE), intent, 0);
            alarmManager.set(AlarmManager.RTC_WAKEUP, event.getDateTime(), pendingIntent);
        }
    }

    /**
     * Cancels alarm for each active event.
     */
    private void stopNotification() {
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        for (Event event : mEvents) {
            Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
            intent.putExtra(AlarmReceiver.ARG_EVENT, event);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getBaseContext(),
                    (int) (event.get_id() % Integer.MAX_VALUE), intent, 0);
            alarmManager.cancel(pendingIntent);
        }
    }

    /**
     * Adapter for ViewPager. Manages tab-fragments.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /**
         * Instantiates the fragment for the given page
         *
         * @param position number of tab
         * @return tab-fragment
         */
        @Override
        public Fragment getItem(int position) {
            Log.i(Constants.LOG_TAG, "создаем новый фрагмент для события "
                    + mEvents.get(position).getName());
            return PlaceholderFragment.newInstance(mEvents.get(position));
        }

        /**
         * Only returns tabs count.
         *
         * @return tabs count.
         */
        @Override
        public int getCount() {
            return mEventsCount;
        }

        /**
         * Returns name for the event on position.
         *
         * @param position number of tab.
         * @return event name.
         */
        @Override
        public CharSequence getPageTitle(int position) {
            return mEvents.get(position).getName();
        }
    }

    /**
     * Tab-fragment for event displaying.
     * It contents event object and timer for countdown. When fragment is showed, it counts down
     * the time remaining before the event and starts the timer. Timer decreases the time every
     * second. When we need to refill fragments, timer stops and then start again, but on refresh
     * fragment just refill components. In the fragment each digit of time shows in individual
     * TextView, so the fragment must also manage their style.
     */
    public static class PlaceholderFragment extends Fragment {

        private static final String ARG_EVENT = "event";
        private long mTimeLeft = 0;
        private Handler handler;
        private MainActivity mActivity;
        private Timer timer;
        private Event mEvent;

        /**
         * Sets the event to the arguments. Creates new tab-fragment.
         *
         * @param event the event for displaying.
         * @return tab-fragment.
         */
        public static PlaceholderFragment newInstance(Event event) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putParcelable(ARG_EVENT, event);
            fragment.setArguments(args);
            Log.i(Constants.LOG_TAG, "добавили в аргументы фрагмента событие" + event.getName());
            return fragment;
        }

        /**
         * Empty.
         */
        public PlaceholderFragment() {

        }

        /**
         * Inflates layout, shows event text and remaining time and start the timer.
         *
         * @param inflater           for inflate
         * @param container          for inflate
         * @param savedInstanceState not used
         * @return filled tab
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            final View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            viewEvent(rootView);
            startCounter();
            return rootView;
        }

        /**
         * Sets font family and color of TextView.
         *
         * @param view      parent view.
         * @param id        id of TextView.
         * @param textColor color to set.
         */
        //меняет шрифт и цвет текста у TextView
        private void applyStyle(View view, int id, @ColorInt int textColor) {
            TextView textView = (TextView) view.findViewById(id);
            textView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                    "Azeroth Cyrillic.ttf"));
            textView.setTextColor(textColor);
        }

        /**
         * Displays event's properties and remaining time on tab.
         *
         * @param view tab's view.
         */
        //устанавливает фон вкладки, выводит текст и оставшееся время
        private void viewEvent(View view) {
            if (mEvent != null) {
                Log.i(Constants.LOG_TAG, "выводим данные события " + mEvent.getName());
                //устанавливаем фон
                /*ImageView ivBackground = (ImageView) view.findViewById(R.id.ivBackground);
                ivBackground.setImageResource(EventStyle.getImageId(mEvent.getEventStyle()));*/
                //фон берется из события
                LinearLayout container = (LinearLayout) view
                        .findViewById(R.id.layoutEventContainer);
                container.setBackgroundResource(EventStyle.getImageId(mEvent.getEventStyle()));

                //выводим текст события
                //текст берется из события
                TextView textView = (TextView) view.findViewById(R.id.labelText);
                textView.setText(mEvent.getText());
                //шрифт одинаковый для всех
                textView.setTypeface(Typeface.createFromAsset(getActivity().getAssets(),
                        "Azeroth Cyrillic.ttf"));
                //цвет текста берется из события
                int textColor = getResources().getColor(EventStyle
                        .getToolbarColor(mEvent.getEventStyle()));
                textView.setTextColor(textColor);

                //textColor = Color.BLACK;
                applyStyle(view, R.id.textDays, textColor);
                applyStyle(view, R.id.textHours1, textColor);
                applyStyle(view, R.id.textHours2, textColor);
                applyStyle(view, R.id.textMinutes1, textColor);
                applyStyle(view, R.id.textMinutes2, textColor);
                applyStyle(view, R.id.textSeconds1, textColor);
                applyStyle(view, R.id.textSeconds2, textColor);
                applyStyle(view, R.id.labelDays, textColor);
                applyStyle(view, R.id.labelHours, textColor);
                applyStyle(view, R.id.labelMinutes, textColor);
                long now = new Date().getTime();
                long difference = mEvent.getDateTime() - now;
                if (difference <= 0) difference = 0;
                difference = difference / 1000;
                mTimeLeft = difference;
            } else {
                mTimeLeft = 0;
            }
        }

        /**
         * Starts timer
         */
        public void startCounter() {
            long now = new Date().getTime();
            long difference = mEvent.getDateTime() - now;
            if (difference <= 0) difference = 0;
            difference = difference / 1000;
            mTimeLeft = difference;
            if (timer != null) {
                Log.i(Constants.LOG_TAG, "Timer is not null!");
                stopCounter();
            }
            handler = new Handler();
            //таймер будет каждую секунду убавлять оставшееся до события время на 1 секунду
            //и вызывать хандлер чтобы вывести новое время на вью
            timer = new Timer();
            timer.schedule(new TimerTask() {
                // Определяем задачу
                @Override
                public void run() {
                    if (mTimeLeft == 0) {
                        timer.cancel();
                        timer.purge();
                    }
                    Log.i(Constants.LOG_TAG, toString());
                    long difference = mTimeLeft;
                    final int seconds = (int) difference % 60;
                    difference = difference / 60;
                    final int minutes = (int) difference % 60;
                    difference = difference / 60;
                    final int hours = (int) difference % 24;
                    final int days = (int) difference / 24;
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            View view = getView();
                            //поскольку таймер выполняется в параллельном потоке, возможна ситуация
                            //когда вью уже нет, а таймер все еще тикает
                            if (view != null) {
                                TextView tvDays = (TextView) view.findViewById(R.id.textDays);
                                tvDays.setText(String.format("%1$d", days));
                                TextView labelDays = (TextView) view.findViewById(R.id.labelDays);
                                int tmpDays = days % 10;
                                if (tmpDays == 1) {
                                    labelDays.setText(getString(R.string.labelDays_text1));
                                } else if ((tmpDays >= 2) && (tmpDays <= 4)) {
                                    labelDays.setText(getString(R.string.labelDays_text2));
                                } else {
                                    labelDays.setText(getString(R.string.labelDays_text0));
                                }
                                TextView tvHours1 = (TextView) view.findViewById(R.id.textHours1);
                                tvHours1.setText(String.format("%1$d", hours / 10));
                                TextView tvHours2 = (TextView) view.findViewById(R.id.textHours2);
                                tvHours2.setText(String.format("%1$d", hours % 10));
                                TextView tvMinutes1 = (TextView) view
                                        .findViewById(R.id.textMinutes1);
                                tvMinutes1.setText(String.format("%1$d", minutes / 10));
                                TextView tvMinutes2 = (TextView) view
                                        .findViewById(R.id.textMinutes2);
                                tvMinutes2.setText(String.format("%1$d", minutes % 10));
                                TextView tvSeconds1 = (TextView) view
                                        .findViewById(R.id.textSeconds1);
                                tvSeconds1.setText(String.format("%1$d", seconds / 10));
                                TextView tvSeconds2 = (TextView) view
                                        .findViewById(R.id.textSeconds2);
                                tvSeconds2.setText(String.format("%1$d", seconds % 10));
                                mTimeLeft--;
                            }
                        }
                    });
                }

            }, 0L, 1000);
        }

        /**
         * Gets event from arguments, initializes variables if screen was rotated,
         * adds fragment to fragment array
         *
         * @param context MainActivity for using it's methods.
         */
        @Override
        public void onAttach(Context context) {
            super.onAttach(context);
            mEvent = getArguments().getParcelable(ARG_EVENT);
            mActivity = (MainActivity) context;

            //на случай если экран перевернули и все значения потерялись
            mActivity.initEventFragments();

            mActivity.addFragment(this);
            Log.i(Constants.LOG_TAG, "приаттачили фрагмент " + mEvent.getName());
        }

        /**
         * Does nothing.
         */
        @Override
        public void onDetach() {
            //происходит только при выходе из приложения
            Log.i(Constants.LOG_TAG, "детач фрагмента " + mEvent.getName());
            super.onDetach();
        }

        /**
         * Stops timer
         */
        public void stopCounter() {
            if (timer != null) {
                timer.cancel();
                timer.purge();
                timer = null;
                Log.i(Constants.LOG_TAG, "обнулили таймер фрагмент " + mEvent.getName());
            }
        }

        /**
         * Sets event from params to fragment and puts it to the arguments.
         * Then calls {@Link #viewEvent}.
         *
         * @param event new event for displaying
         */
        public void update(Event event) {
            Bundle args = getArguments();
            args.putParcelable(ARG_EVENT, event);
            mEvent = event;
            View view = getView();
            if (view != null) {
                viewEvent(view);
            }
        }

        /**
         * Only returns event name.
         *
         * @return event name
         */
        public String getEventName() {
            return mEvent.getName();
        }
    }
}
