package biz.no_ip.palaven.countdown;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * List fragment with events. Binds to the EventListActivity.
 *
 * @author Aliya Gainetdinova
 */
public class EventListFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";

    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;
    private OnActiveChangeListener mActiveChangeListener;
    private RecyclerView mRecyclerView;

    /**
     * Empty.
     */
    public EventListFragment() {
    }

    /**
     * Creates new object.
     *
     * @param columnCount 1
     * @return new fragment.
     */
    @SuppressWarnings("unused")
    public static EventListFragment newInstance(int columnCount) {
        EventListFragment fragment = new EventListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Gets column count from the arguments.
     *
     * @param savedInstanceState used in parent's method.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    /**
     * Creates new RVAdapter with all events list and binds it to the RecyclerView.
     *
     * @param inflater           to inflate view
     * @param container          to inflate view
     * @param savedInstanceState not used
     * @return view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_event_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            mRecyclerView = (RecyclerView) view;
            registerForContextMenu(mRecyclerView);
            if (mColumnCount <= 1) {
                mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                mRecyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            Database db = new Database(context);
            mRecyclerView.setAdapter(new RVAdapter(db.getEventList(), mListener,
                    mActiveChangeListener));
        }
        return view;
    }

    /**
     * Binds the EventListActivity to process list actions.
     *
     * @param context EventListActivity
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
            mActiveChangeListener = (OnActiveChangeListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener " +
                    "or OnActiveChangeListener");
        }
    }

    /**
     * Unbind the activity.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * Creates new RVAdapter and binds it to the RecyclerView.
     */
    public void update() {
        Database db = new Database(getContext());
        mRecyclerView.setAdapter(new RVAdapter(db.getEventList(), mListener,
                mActiveChangeListener));
    }

    /**
     * Gets bound adapter.
     *
     * @return adapter
     */
    public RVAdapter getAdapter() {
        return (RVAdapter) mRecyclerView.getAdapter();
    }

    /**
     * An interface to connect fragment with activity. Transfers list item click action.
     */
    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(Event event);
    }

    /**
     * An interface to connect fragment with activity. Transfers active change action.
     */
    public interface OnActiveChangeListener {
        void onActiveChange(Event event, boolean active);
    }
}
