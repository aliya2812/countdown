package biz.no_ip.palaven.countdown;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Class provides the database
 *
 * @author Aliya Gainetdinova
 */
public class Database {

    private DatabaseHelper mDBHelper;

    /**
     * Creates DatabaseHelper object
     *
     * @param context for DatabaseHelper constructor
     */
    public Database(Context context) {
        mDBHelper = new DatabaseHelper(context);
    }

    /**
     * Reads all events from database and writes it to List collection
     *
     * @return List of all the events from database
     */
    public List<Event> getEventList() {
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        List<Event> eventList = new ArrayList<>();
        try {
            // Iterate events
            QueryResultIterable<Event> itr = cupboard().withDatabase(db).query(Event.class).query();
            for (Event event : itr) {
                eventList.add(event);
            }
            itr.close();
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, "Can't read events from database");
        }

        return eventList;
    }

    /**
     * Reads events which Active-field is 1 from database and writes them to the List
     *
     * @return List of active events from database
     */
    public List<Event> getActiveEventList() {
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        List<Event> eventList = new ArrayList<>();
        try {
            QueryResultIterable<Event> itr = cupboard().withDatabase(db).query(Event.class)
                    .withSelection("active = ?", "1").query();
            for (Event event : itr) {
                eventList.add(event);
            }
            itr.close();
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, "Can't read events from database");
        }

        return eventList;
    }

    /**
     * Adds new record to the database. Fields are filled from the Event object
     *
     * @param event Event object to add to database
     * @return new record id
     */
    public long addEvent(Event event) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        return cupboard().withDatabase(db).put(event);
    }

    /**
     * Updates record in database table. Id for searching record is id of event param.
     * Fields are filled from it too.
     *
     * @param event Event object for replace in database
     */
    public void updateEvent(Event event) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        cupboard().withDatabase(db).update(Event.class, eventToContentValues(event));
        db.close();
    }

    /**
     * Removes record from database table. Record is searched by id of event param
     *
     * @param event event to remove
     */
    public void removeEvent(Event event) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        cupboard().withDatabase(db).delete(Event.class, event.get_id());
        db.close();
    }

    /**
     * Gets event from database by id.
     *
     * @param id id to search the event
     * @return found event
     */
    public Event getEvent(long id) {
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        Event event = cupboard().withDatabase(db).get(Event.class, id);
        db.close();
        return event;
    }

    /**
     * Check event's id. Adds new event or updates existing.
     *
     * @param event event to save
     */
    public void saveEvent(Event event) {
        if (event.get_id() == null) {
            addEvent(event);
        } else {
            updateEvent(event);
        }
    }

    /**
     * Converts Event object to ContentValues
     *
     * @param event event to convert
     * @return ContentValues with filled fields.
     */
    private ContentValues eventToContentValues(Event event) {
        return cupboard().withEntity(Event.class).toContentValues(event);
    }
}
