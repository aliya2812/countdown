package biz.no_ip.palaven.countdown;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;

import java.util.Date;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in
 * {@link EventWidgetConfigureActivity EventWidgetConfigureActivity}
 */
public class EventWidget extends AppWidgetProvider {

    public static final String ACTION = "text_click";
    public static final String ARG_ID = "event_id";

    /**
     * Gets the event from preferences and shows it's properties.
     *
     * @param context          context to get the preferences
     * @param appWidgetManager system widget manager
     * @param appWidgetId      widget id
     */
    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        Event event = EventWidgetConfigureActivity.loadEventPref(context, appWidgetId);
        if (event != null) {
            // Construct the RemoteViews object
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.event_widget);
            views.setImageViewResource(R.id.appwidget_background,
                    EventStyle.getImageId(event.getEventStyle()));
            views.setTextViewText(R.id.appwidget_text, event.getText() + " " +
                    getTimeRemaining(event.getDateTime(), context));
            views.setTextColor(R.id.appwidget_text, context.getResources().getColor(EventStyle
                    .getToolbarColor(event.getEventStyle())));
            Intent intent = new Intent(context, EventWidget.class);
            intent.setAction(ACTION);
            intent.putExtra(ARG_ID, event.get_id());
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, appWidgetId, intent, 0);
            views.setOnClickPendingIntent(R.id.appwidget_text, pendingIntent);

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            EventWidgetConfigureActivity.deleteEventPref(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (ACTION.equals(intent.getAction())) {
            long event_id = intent.getLongExtra(ARG_ID, 0);
            Intent newIntent = new Intent(context, MainActivity.class);
            newIntent.putExtra(MainActivity.ARG_EVENTID, event_id);
            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(newIntent);
        }
        super.onReceive(context, intent);
    }

    /**
     * Builds the String with two first parameters of remaining time. For example: "2 days 1 hour"
     * or "15 hours 58 minutes" or "32 seconds"
     *
     * @param time    event's datetime
     * @param context context to get strings
     * @return text with remaining time
     */
    static private String getTimeRemaining(long time, Context context) {
        long timeRemaining = (time - new Date().getTime()) / 1000;
        if (timeRemaining < 0) timeRemaining = 0;
        final int seconds = (int) timeRemaining % 60;
        timeRemaining = timeRemaining / 60;
        final int minutes = (int) timeRemaining % 60;
        timeRemaining = timeRemaining / 60;
        final int hours = (int) timeRemaining % 24;
        final int days = (int) timeRemaining / 24;
        StringBuilder timeStrBuilder = new StringBuilder();
        if (days > 0) {
            timeStrBuilder.append(Integer.toString(days));
            timeStrBuilder.append(" ");
            int tmpDays = days % 10;
            if (tmpDays == 1) {
                timeStrBuilder.append(context.getString(R.string.labelDays_text1));
            } else if ((tmpDays >= 2) && (tmpDays <= 4)) {
                timeStrBuilder.append(context.getString(R.string.labelDays_text2));
            } else {
                timeStrBuilder.append(context.getString(R.string.labelDays_text0));
            }
            timeStrBuilder.append(" ");
            timeStrBuilder.append(Integer.toString(hours));
            timeStrBuilder.append(" ");
            int tmpHours = hours % 10;
            if (tmpHours == 1) {
                timeStrBuilder.append(context.getString(R.string.hours_text1));
            } else if ((tmpHours >= 2) && (tmpHours <= 4)) {
                timeStrBuilder.append(context.getString(R.string.hours_text2));
            } else {
                timeStrBuilder.append(context.getString(R.string.hours_text0));
            }
        } else if (hours > 0) {
            timeStrBuilder.append(Integer.toString(hours));
            timeStrBuilder.append(" ");
            int tmpHours = hours % 10;
            if (tmpHours == 1) {
                timeStrBuilder.append(context.getString(R.string.hours_text1));
            } else if ((tmpHours >= 2) && (tmpHours <= 4)) {
                timeStrBuilder.append(context.getString(R.string.hours_text2));
            } else {
                timeStrBuilder.append(context.getString(R.string.hours_text0));
            }
            timeStrBuilder.append(" ");
            timeStrBuilder.append(Integer.toString(minutes));
            timeStrBuilder.append(" ");
            int tmpMinutes = minutes % 10;
            if (tmpMinutes == 1) {
                timeStrBuilder.append(context.getString(R.string.minutes_text1));
            } else if ((tmpMinutes >= 2) && (tmpMinutes <= 4)) {
                timeStrBuilder.append(context.getString(R.string.minutes_text2));
            } else {
                timeStrBuilder.append(context.getString(R.string.minutes_text0));
            }
        } else if (minutes > 0) {
            timeStrBuilder.append(Integer.toString(minutes));
            timeStrBuilder.append(" ");
            int tmpMinutes = minutes % 10;
            if (tmpMinutes == 1) {
                timeStrBuilder.append(context.getString(R.string.minutes_text1));
            } else if ((tmpMinutes >= 2) && (tmpMinutes <= 4)) {
                timeStrBuilder.append(context.getString(R.string.minutes_text2));
            } else {
                timeStrBuilder.append(context.getString(R.string.minutes_text0));
            }
            timeStrBuilder.append(" ");
            timeStrBuilder.append(Integer.toString(seconds));
            timeStrBuilder.append(" ");
            int tmpSeconds = seconds % 10;
            if (tmpSeconds == 1) {
                timeStrBuilder.append(context.getString(R.string.seconds_text1));
            } else if ((tmpSeconds >= 2) && (tmpSeconds <= 4)) {
                timeStrBuilder.append(context.getString(R.string.seconds_text2));
            } else {
                timeStrBuilder.append(context.getString(R.string.seconds_text0));
            }
        } else {
            timeStrBuilder.append(Integer.toString(seconds));
            timeStrBuilder.append(" ");
            int tmpSeconds = seconds % 10;
            if (tmpSeconds == 1) {
                timeStrBuilder.append(context.getString(R.string.seconds_text1));
            } else if ((tmpSeconds >= 2) && (tmpSeconds <= 4)) {
                timeStrBuilder.append(context.getString(R.string.seconds_text2));
            } else {
                timeStrBuilder.append(context.getString(R.string.seconds_text0));
            }
        }
        return timeStrBuilder.toString();
    }
}

