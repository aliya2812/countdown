package biz.no_ip.palaven.countdown;

/**
 * Contents constants for the entire program
 *
 * @author Aliya Gainetdinova
 */
public class Constants {

    /**
     * Tag for log-writing
     */
    public static String LOG_TAG = "Countdown";
}
