package biz.no_ip.palaven.countdown;

import android.app.Activity;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Fragment for choosing time. Calls the TimePickerDialog and sends chosen time to the EventActivity.
 *
 * @author Aliya Gainetdinova
 */
public class TimePickerDialogFragment extends DialogFragment implements
        TimePickerDialog.OnTimeSetListener {

    public static final String ARG_TIME = "time";

    private TimePickedListener mListener;
    private Calendar mTime;

    /**
     * Gets the time from the arguments, creates TimePickerDialog and gives it the time.
     *
     * @param savedInstanceState not used
     * @return TimePickerDialog with time
     */
    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mTime = (Calendar) getArguments().getSerializable(ARG_TIME);

        if (mTime == null) {
            mTime = Calendar.getInstance();
        }

        int hour = mTime.get(Calendar.HOUR_OF_DAY);
        int minute = mTime.get(Calendar.MINUTE);

        // создадим экземпляр класса DatePickerDialog и вернем его
        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

    /**
     * Attaches EventActivity to the fragment to give it the chosen time
     *
     * @param activity a TimePickedListener activity
     */
    @Override
    public void onAttach(Activity activity) {
        // when the fragment is initially shown (i.e. attached to the activity),
        // cast the activity to the callback interface type
        super.onAttach(activity);
        try {
            mListener = (TimePickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + TimePickedListener.class.getName());
        }
    }

    /**
     * Sends picked time to the activity. Seconds are always 0.
     *
     * @param view      not used
     * @param hourOfDay picked hour
     * @param minute    picked minute
     */
    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        // Пользователь выбрал дату. Выводим его в текстовой метке
        mTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mTime.set(Calendar.MINUTE, minute);
        mTime.set(Calendar.SECOND, 0);

        mListener.onTimePicked(mTime);
    }

    /**
     * An interface to connect fragment with activity
     */
    public interface TimePickedListener {
        void onTimePicked(Calendar time);
    }
}
