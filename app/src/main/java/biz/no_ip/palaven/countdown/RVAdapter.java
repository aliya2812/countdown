package biz.no_ip.palaven.countdown;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * List adapter for the EventListFragment.
 *
 * @author Aliya Gainetdinova
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.EventViewHolder> {

    public static final int IDM_DELETE = 201;

    /**
     * View for each event in list. Contains event name, datetime and active switch.
     */
    public static class EventViewHolder extends RecyclerView.ViewHolder
            implements View.OnCreateContextMenuListener {

        public final CardView cv;
        public Event event;
        public final TextView eventName;
        public final TextView eventDateTime;
        public final Switch eventActive;

        EventViewHolder(View itemView) {
            super(itemView);
            cv = (CardView) itemView.findViewById(R.id.itemEvents);
            eventName = (TextView) itemView.findViewById(R.id.labelName);
            eventDateTime = (TextView) itemView.findViewById(R.id.labelDateTime);
            eventActive = (Switch) itemView.findViewById(R.id.switchActive);
        }

        /**
         * Creates context menu for item. The menu consists of one element: delete.
         *
         * @param menu     ContextMenu to add elements
         * @param v        not used
         * @param menuInfo not used
         */
        @Override
        public void onCreateContextMenu(ContextMenu menu, View v,
                                        ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(0, RVAdapter.IDM_DELETE, 0, R.string.delete);
        }
    }

    private List<Event> mEvents;
    private EventListFragment.OnListFragmentInteractionListener mListener;
    private EventListFragment.OnActiveChangeListener mActiveChangeListener;
    private long mEventId;

    /**
     * Gets events list from parameters and binds EventListActivity to process list actions.
     *
     * @param items                events list
     * @param listener             EventListActivity
     * @param activeChangeListener EventListActivity
     */
    public RVAdapter(List<Event> items,
                     EventListFragment.OnListFragmentInteractionListener listener,
                     EventListFragment.OnActiveChangeListener activeChangeListener) {
        mEvents = items;
        this.mListener = listener;
        this.mActiveChangeListener = activeChangeListener;
    }

    /**
     * Creates view for each item using fragment_item layout.
     *
     * @param viewGroup the RecyclerView
     * @param i         not used
     * @return EventViewHolder
     */
    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.fragment_item,
                viewGroup, false);
        return new EventViewHolder(v);
    }

    /**
     * Fills the EventViewHolder for event. Binds onClick method to the
     * {Link #onListFragmentInteraction()}, onCheckedChanged method to
     * {Link #onActiveChange()}. The onLongClick method saves current item id to act with it later.
     *
     * @param eventViewHolder eventViewHolder to fill
     * @param i               current event's position in the list
     */
    @Override
    public void onBindViewHolder(final EventViewHolder eventViewHolder, int i) {
        eventViewHolder.event = mEvents.get(i);
        eventViewHolder.eventName.setText(eventViewHolder.event.getName());
        eventViewHolder.eventDateTime.setText(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
                .format(eventViewHolder.event.getDateTime()));
        eventViewHolder.eventActive.setChecked(eventViewHolder.event.getActive());

        eventViewHolder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(eventViewHolder.event);
                }
            }
        });
        eventViewHolder.cv.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                setEventId(eventViewHolder.event.get_id());
                return false;
            }
        });

        eventViewHolder.cv.setOnCreateContextMenuListener(eventViewHolder);

        eventViewHolder.eventActive.setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (mActiveChangeListener != null) {
                            mActiveChangeListener.onActiveChange(eventViewHolder.event, isChecked);
                        }
                    }
                });
    }

    /**
     * Unbinds the activity.
     *
     * @param holder used in parent's method
     */
    @Override
    public void onViewRecycled(EventViewHolder holder) {
        holder.cv.setOnLongClickListener(null);
        super.onViewRecycled(holder);
    }

    @Override
    public int getItemCount() {
        return mEvents.size();
    }

    public long getEventId() {
        return mEventId;
    }

    public void setEventId(long eventId) {
        this.mEventId = eventId;
    }
}
