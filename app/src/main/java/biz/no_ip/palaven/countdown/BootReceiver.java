package biz.no_ip.palaven.countdown;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.List;

/**
 * Launches alarms for events when Android started.
 */
public class BootReceiver extends BroadcastReceiver {
    Context mContext;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        String BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";
        String action = intent.getAction();
        if (action.equalsIgnoreCase(BOOT_ACTION)) {
            Database db = new Database(context);
            List<Event> eventList = db.getActiveEventList();
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(
                    Context.ALARM_SERVICE);
            for (Event event : eventList) {
                Intent intent1 = new Intent(context, AlarmReceiver.class);
                intent1.putExtra(AlarmReceiver.ARG_EVENT, event);
                PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
                        (int) (event.get_id() % Integer.MAX_VALUE), intent1, 0);
                alarmManager.set(AlarmManager.RTC_WAKEUP, event.getDateTime(), pendingIntent);
            }
        }
    }
}
