package biz.no_ip.palaven.countdown;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;

/**
 * Activity with events list.
 *
 * @author Aliya Gainetdinova
 */
public class EventListActivity extends AppCompatActivity
        implements EventListFragment.OnListFragmentInteractionListener,
        EventListFragment.OnActiveChangeListener {

    /**
     * Initializes Add button. It calls EventActivity without arguments.
     *
     * @param savedInstanceState used only in parent onCreate
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.btnAddEvent);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(EventListActivity.this, EventActivity.class);
                startActivityForResult(intent, EventActivity.ADD_EVENT_REQUEST);
            }
        });
    }

    /**
     * Gets current item id from list adapter. If user selected to delete item, calls
     * {@Link #removeEvent} for the item id.
     *
     * @param item selected context menu item
     * @return parent's onContextItemSelected() result
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        FragmentManager manager = getSupportFragmentManager();
        EventListFragment fragment = (EventListFragment) manager.findFragmentById(R.id.fragment);
        long eventId;
        try {
            eventId = (fragment.getAdapter()).getEventId();
        } catch (Exception e) {
            return super.onContextItemSelected(item);
        }
        switch (item.getItemId()) {
            case RVAdapter.IDM_DELETE:
                Database db = new Database(EventListActivity.this);
                db.removeEvent(db.getEvent(eventId));
                fragment.update();
                break;
        }
        return super.onContextItemSelected(item);
    }

    /**
     * Calls EventActivity with clicked event as parameter
     *
     * @param event clicked event
     */
    @Override
    public void onListFragmentInteraction(Event event) {
        Intent intent = new Intent(EventListActivity.this, EventActivity.class);
        intent.putExtra(EventActivity.ARG_EVENT, event);
        startActivityForResult(intent, EventActivity.EDIT_EVENT_REQUEST);
    }

    /**
     * Changes Active field for current event and writes it to the database table.
     *
     * @param event  current event
     * @param active new value for active field
     */
    @Override
    public void onActiveChange(Event event, boolean active) {
        event.setActive(active);
        Database db = new Database(EventListActivity.this);
        db.saveEvent(event);
    }

    /**
     * If event was saved in the EventActivity, list updates.
     *
     * @param requestCode EDIT_EVENT_REQUEST or ADD_EVENT_REQUEST
     * @param resultCode  OK or some other
     * @param data        used only in parent's method
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if ((requestCode == EventActivity.EDIT_EVENT_REQUEST)
                || (requestCode == EventActivity.ADD_EVENT_REQUEST)) {
            if (resultCode == RESULT_OK) {
                FragmentManager fragmentManager = getSupportFragmentManager();
                EventListFragment fragment = (EventListFragment) fragmentManager
                        .findFragmentById(R.id.fragment);
                fragment.update();
            }
        }
    }
}
