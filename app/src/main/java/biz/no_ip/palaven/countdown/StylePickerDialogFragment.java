package biz.no_ip.palaven.countdown;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

/**
 * Fragment for choosing style. Calls the List Dialog and sends chosen style to the EventActivity.
 *
 * @author Aliya Gainetdinova
 */
public class StylePickerDialogFragment extends DialogFragment implements
        AdapterView.OnItemClickListener {

    public static final String ARG_STYLE = "eventStyle";

    private StylePickedListener mListener;
    private int mEventStyle;

    String[] stylesArray = EventStyle.getStylesArray();

    /**
     * Gets the style from the arguments, creates Dialog.
     *
     * @param savedInstanceState not used
     * @return Dialog
     */
    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mEventStyle = getArguments().getInt(ARG_STYLE, -1);

        Dialog dialog = new Dialog(getActivity());
        dialog.setTitle(R.string.choose_style);
        return dialog;
    }

    /**
     * Inflates Dialog from fragment_style_picker_dialog layout. Sets StyleAdapter to fill it.
     * List contents all the constants from EventStyle.
     *
     * @param inflater           to inflate dialog
     * @param container          to inflate dialog
     * @param savedInstanceState not used
     * @return dialog view
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_style_picker_dialog, container, false);
        ListView listView = (ListView) view.findViewById(R.id.listStyles);
        listView.setAdapter(new StyleAdapter(getContext()));
        listView.setOnItemClickListener(this);
        if (mEventStyle >= 0) {
            listView.setSelection(mEventStyle);
        }
        return view;
    }

    /**
     * Sends picked style to the Activity and closes the dialog.
     *
     * @param parent   not used
     * @param view     not used
     * @param position EventStyle constant
     * @param id       not used
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mListener != null) {
            mListener.onStylePicked(position);
            dismiss();
        }
    }

    /**
     * Attaches EventActivity to the fragment to give it the chosen style
     *
     * @param activity a TimePickedListener activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (activity instanceof StylePickedListener) {
            mListener = (StylePickedListener) activity;
        } else {
            throw new RuntimeException(activity.toString()
                    + " must implement StylePickedListener");
        }
    }

    /**
     * Unbind the activity.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * An interface to connect fragment with activity
     */
    public interface StylePickedListener {
        void onStylePicked(int eventStyle);
    }

    /**
     * Adapter to fill the list in the dialog. Each list item presented by mini background image
     * and event name.
     */
    public class StyleAdapter extends BaseAdapter {
        private LayoutInflater mLayoutInflater;

        public StyleAdapter(Context context) {
            mLayoutInflater = LayoutInflater.from(context);
        }

        public int getCount() {
            return stylesArray.length;
        }

        public Object getItem(int position) {
            return position;
        }

        public long getItemId(int position) {
            return position;
        }

        /**
         * Inflates each item from style_list_item layout and shows style's icon and style's name.
         *
         * @param position    EventStyle constant
         * @param convertView item view
         * @param parent      not used
         * @return item view
         */
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = mLayoutInflater.inflate(R.layout.style_list_item, null);

            ImageView image = (ImageView) convertView.findViewById(R.id.imageBackPicture);
            image.setImageResource(EventStyle.getIconId(position));

            TextView signTextView = (TextView) convertView.findViewById(R.id.textStyle);
            signTextView.setText(stylesArray[position]);

            return convertView;
        }
    }
}
