package biz.no_ip.palaven.countdown;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

/**
 * Fragment for choosing date. Calls the DatePickerDialog and sends chosen date to the EventActivity.
 *
 * @author Aliya Gainetdinova
 */
public class DatePickerDialogFragment extends DialogFragment implements
        DatePickerDialog.OnDateSetListener {

    public static final String ARG_DATE = "date";

    private DatePickedListener mListener;
    private Calendar mDate;

    /**
     * Gets the date from the arguments, creates DatePickerDialog and gives it the date.
     *
     * @param savedInstanceState not used
     * @return DatePickerDialog with date
     */
    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        mDate = (Calendar) getArguments().getSerializable(ARG_DATE);
        if (mDate == null) {
            mDate = Calendar.getInstance();
        }

        // установим текущую дату в диалоговом окне
        int year = mDate.get(Calendar.YEAR);
        int month = mDate.get(Calendar.MONTH);
        int day = mDate.get(Calendar.DAY_OF_MONTH);

        // создадим экземпляр класса DatePickerDialog и вернем его
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    /**
     * Attaches EventActivity to the fragment to give it the chosen date
     *
     * @param activity a DatePickedListener activity
     */
    @Override
    public void onAttach(Activity activity) {
        // when the fragment is initially shown (i.e. attached to the activity),
        // cast the activity to the callback interface type
        super.onAttach(activity);
        try {
            mListener = (DatePickedListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement " + DatePickedListener.class.getName());
        }
    }

    /**
     * Sends picked date to the activity.
     *
     * @param view  not used
     * @param year  picked year
     * @param month picked month
     * @param day   picked day
     */
    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Пользователь выбрал дату. Выводим его в текстовой метке
        mDate.set(Calendar.YEAR, year);
        mDate.set(Calendar.MONTH, month);
        mDate.set(Calendar.DAY_OF_MONTH, day);

        mListener.onDatePicked(mDate);
    }

    /**
     * An interface to connect fragment with activity
     */
    public interface DatePickedListener {
        void onDatePicked(Calendar time);
    }
}
