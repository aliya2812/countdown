package biz.no_ip.palaven.countdown;


import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;

/**
 * Describes skins for events. Int constants are for storage in the database. String represents and
 * icons are for the StylePickerDialog. Images are for backgrounds in MainActivity. Colors are the
 * font colors in MainActivity.
 *
 * @author Aliya Gainetdinova
 */
public class EventStyle {

    public static final int SUMMER = 0;//?
    public static final int CHRISTMAS = 1;//+
    //public static final int NEWYEAR = 2;//-
    //public static final int BIRTHDAY = 3;//-
    //public static final int SNOW = 4;//-
    public static final int GRASS = 2;//5+
    public static final int FLOWER = 3;//6+
    //public static final int DEERS = 7;//-
    public static final int BUTTERFLY = 4;//8?
    public static final int CHRISTMASFRAME = 5;//9+
    public static final int CHRISTMASTOWN = 6;//10?
    public static final int FALLINGHEARTS = 7;//11+
    public static final int GREEN = 8;//12+
    public static final int HALLOWEEN = 9;//13?
    public static final int HEART = 10;//14?
    public static final int PUMPKIN = 11;//15?
    public static final int SEA = 12;//16+
    public static final int VALENTINERED = 13;//17?
    public static final int PINK = 14;//18?
    public static final int BIRD = 15;//19+
    public static final int BLUE = 16;//20+
    public static final int CLOCK = 17;//21+
    public static final int FISH = 18;//22?
    public static final int GLOWINGPINETREE = 19;//23?
    public static final int IVORY = 20;//24+
    public static final int LEAF = 21;//25?
    public static final int PINETREE = 22;//26+
    public static final int REDSNOW = 23;//27+
    public static final int NEWYEAR = 24;
    public static final int BIRTHDAY = 25;
    public static final int FLAME = 26;
    public static final int TREE = 27;

    /**
     * Returns array with all the style's names for listing in StylePickerDialog.
     *
     * @return array with style's names
     */
    public static String[] getStylesArray() {
        return new String[]{getStyle(SUMMER), getStyle(CHRISTMAS), /*getStyle(NEWYEAR),*/
                /*getStyle(BIRTHDAY),*/ /*getStyle(SNOW),*/ getStyle(GRASS), getStyle(FLOWER),
                /*getStyle(DEERS),*/ getStyle(BUTTERFLY), getStyle(CHRISTMASFRAME),
                getStyle(CHRISTMASTOWN), getStyle(FALLINGHEARTS), getStyle(GREEN),
                getStyle(HALLOWEEN), getStyle(HEART), getStyle(PUMPKIN), getStyle(SEA),
                getStyle(VALENTINERED), getStyle(PINK), getStyle(BIRD), getStyle(BLUE),
                getStyle(CLOCK), getStyle(FISH), getStyle(GLOWINGPINETREE), getStyle(IVORY),
                getStyle(LEAF), getStyle(PINETREE), getStyle(REDSNOW), getStyle(NEWYEAR),
                getStyle(BIRTHDAY), getStyle(FLAME), getStyle(TREE)};
    }

    /**
     * Returns style name.
     *
     * @param num style constant
     * @return style name
     */
    @NonNull
    public static String getStyle(int num) {
        switch (num) {
            case SUMMER:
                return "Summer";
            case CHRISTMAS:
                return "Christmas";
            /*case NEWYEAR:
                return "New Year";*/
            /*case BIRTHDAY:
                return "Birthday";*/
            /*case SNOW:
                return "Snow";*/
            case GRASS:
                return "Grass";
            case FLOWER:
                return "Flower";
            /*case DEERS:
                return "Deers";*/
            case BUTTERFLY:
                return "Butterfly";
            case CHRISTMASFRAME:
                return "Christmas frame";
            case CHRISTMASTOWN:
                return "Christmas town";
            case FALLINGHEARTS:
                return "Falling hearts";
            case GREEN:
                return "Green";
            case HALLOWEEN:
                return "Halloween";
            case HEART:
                return "Heart";
            case PUMPKIN:
                return "Pumpkin";
            case SEA:
                return "Sea";
            case VALENTINERED:
                return "Valentine red";
            case PINK:
                return "Pink";
            case BIRD:
                return "Bird";
            case BLUE:
                return "Blue";
            case CLOCK:
                return "Clock";
            case FISH:
                return "Fish";
            case GLOWINGPINETREE:
                return "Glowing pine tree";
            case IVORY:
                return "Ivory";
            case LEAF:
                return "Leaf";
            case PINETREE:
                return "Pine tree";
            case REDSNOW:
                return "Red snow";
            case NEWYEAR:
                return "New year";
            case BIRTHDAY:
                return "Birthday";
            case FLAME:
                return "Flame";
            case TREE:
                return "Tree";
        }
        return "";
    }

    /**
     * Returns background image id.
     *
     * @param eventStyle style constant
     * @return image id
     */
    @DrawableRes
    public static int getImageId(int eventStyle) {
        switch (eventStyle) {
            case SUMMER:
                return R.drawable.summer;
            case CHRISTMAS:
                return R.drawable.christmas;
            /*case NEWYEAR:
                return R.drawable.newyear;*/
            /*case SNOW:
                return R.drawable.snow;*/
            case GRASS:
                return R.drawable.grass;
            case FLOWER:
                return R.drawable.flower;
            /*case DEERS:
                return R.drawable.deers;*/
            case BUTTERFLY:
                return R.drawable.butterfly;
            case CHRISTMASFRAME:
                return R.drawable.christmasframe;
            case CHRISTMASTOWN:
                return R.drawable.christmastown;
            case FALLINGHEARTS:
                return R.drawable.fallinghearts;
            case GREEN:
                return R.drawable.green;
            case HALLOWEEN:
                return R.drawable.halloween;
            case HEART:
                return R.drawable.heart;
            case PUMPKIN:
                return R.drawable.pumpkin;
            case SEA:
                return R.drawable.sea;
            case VALENTINERED:
                return R.drawable.valentinered;
            case PINK:
                return R.drawable.valentinetemplate;
            case BIRD:
                return R.drawable.bird;
            case BLUE:
                return R.drawable.blue;
            case CLOCK:
                return R.drawable.clock;
            case FISH:
                return R.drawable.fish;
            case GLOWINGPINETREE:
                return R.drawable.glowingpinetree;
            case IVORY:
                return R.drawable.ivory;
            case LEAF:
                return R.drawable.leaf;
            case PINETREE:
                return R.drawable.pinetree;
            case REDSNOW:
                return R.drawable.redsnow;
            case NEWYEAR:
                return R.drawable.newyear;
            case BIRTHDAY:
                return R.drawable.birthday;
            case FLAME:
                return R.drawable.flame;
            case TREE:
                return R.drawable.tree;
        }
        return R.drawable.blue;
    }

    /**
     * Returns font color for style.
     *
     * @param eventStyle style constant
     * @return color id
     */
    @ColorRes
    public static int getToolbarColor(int eventStyle) {
        switch (eventStyle) {
            case SUMMER:
                return R.color.summer;
            case CHRISTMAS:
                return R.color.darkblue;
            /*case NEWYEAR:
                return R.color.red;*/
            /*case SNOW:
                return R.color.winter_blue;*/
            case GRASS:
                return R.color.grass;
            case FLOWER:
                return R.color.flower;
            /*case DEERS:
                return R.color.winter_blue;*/
            case BUTTERFLY:
                return R.color.butterfly;
            case CHRISTMASFRAME:
                return R.color.red;
            case CHRISTMASTOWN:
                return R.color.christmastown;
            case FALLINGHEARTS:
                return R.color.fallinghearts;
            case GREEN:
                return R.color.greenstyle;
            case HALLOWEEN:
                return R.color.halloween;
            case HEART:
                return R.color.heart;
            case PUMPKIN:
                return R.color.pumpkin;
            case SEA:
                return R.color.sea;
            case VALENTINERED:
                return R.color.valentinered;
            case PINK:
                return R.color.pinkstyle;
            case PINETREE:
                return R.color.pinetree;
            case BIRD:
                return R.color.bird;
            case BLUE:
                return R.color.darkblue;
            case CLOCK:
                return R.color.clock;
            case FISH:
                return R.color.fish;
            case GLOWINGPINETREE:
                return R.color.glowingpinetree;
            case IVORY:
                return R.color.ivorystyle;
            case LEAF:
                return R.color.leaf;
            case REDSNOW:
                return R.color.redsnow;
            case FLAME:
                return R.color.flame;
        }
        return R.color.black;
    }

    /**
     * Returns mini image id.
     *
     * @param eventStyle style constant
     * @return image id
     */
    @DrawableRes
    public static int getIconId(int eventStyle) {
        switch (eventStyle) {
            case SUMMER:
                return R.drawable.summer_icon;
            case CHRISTMAS:
                return R.drawable.christmas_icon;
            /*case NEWYEAR:
                return R.drawable.newyear_icon;*/
            /*case SNOW:
                return R.drawable.snow;*/
            case GRASS:
                return R.drawable.grass_icon;
            case FLOWER:
                return R.drawable.flower_icon;
            /*case DEERS:
                return R.drawable.deers;*/
            case BUTTERFLY:
                return R.drawable.butterfly_icon;
            case CHRISTMASFRAME:
                return R.drawable.christmasframe_icon;
            case CHRISTMASTOWN:
                return R.drawable.christmastown_icon;
            case FALLINGHEARTS:
                return R.drawable.fallinghearts_icon;
            case GREEN:
                return R.drawable.green_icon;
            case HALLOWEEN:
                return R.drawable.halloween_icon;
            case HEART:
                return R.drawable.heart_icon;
            case PUMPKIN:
                return R.drawable.pumpkin_icon;
            case SEA:
                return R.drawable.sea_icon;
            case VALENTINERED:
                return R.drawable.valentinered_icon;
            case PINK:
                return R.drawable.valentinetemplate_icon;
            case BIRD:
                return R.drawable.bird_icon;
            case BLUE:
                return R.drawable.blue_icon;
            case CLOCK:
                return R.drawable.clock_icon;
            case FISH:
                return R.drawable.fish_icon;
            case GLOWINGPINETREE:
                return R.drawable.glowingpinetree_icon;
            case IVORY:
                return R.drawable.ivory_icon;
            case LEAF:
                return R.drawable.leaf_icon;
            case PINETREE:
                return R.drawable.pinetree_icon;
            case REDSNOW:
                return R.drawable.redsnow_icon;
            case NEWYEAR:
                return R.drawable.newyear_icon;
            case BIRTHDAY:
                return R.drawable.birthday_icon;
            case FLAME:
                return R.drawable.flame_icon;
            case TREE:
                return R.drawable.tree_icon;
        }
        return R.drawable.blue_icon;
    }

}
