package biz.no_ip.palaven.countdown;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Calendar;

import nl.qbusict.cupboard.QueryResultIterable;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Provides database creating and updating.
 *
 * @author Aliya Gainetdinova
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "countdown.db";
    public static final int DATABASE_VERSION = 3;
    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    /**
     * Creates table for Events
     */
    static {
        cupboard().register(Event.class);
    }

    /**
     * Creates table Event, adds 2 test records to events table.
     *
     * @param db database for table creating
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();

        Calendar date1 = Calendar.getInstance();
        date1.set(Calendar.YEAR, date1.get(Calendar.YEAR) + 1);
        date1.set(Calendar.MONTH, Calendar.JANUARY);
        date1.set(Calendar.DATE, 1);
        date1.set(Calendar.HOUR_OF_DAY, 0);
        date1.set(Calendar.MINUTE, 0);
        date1.set(Calendar.SECOND, 0);
        Event newYear = new Event(true, mContext.getString(R.string.test1_name),
                mContext.getString(R.string.test1_text), date1.getTimeInMillis(),
                EventStyle.CHRISTMASFRAME);
        cupboard().withDatabase(db).put(newYear);

        Calendar date2 = Calendar.getInstance();
        date2.set(Calendar.HOUR_OF_DAY, 17);
        date2.set(Calendar.MINUTE, 30);
        date2.set(Calendar.SECOND, 0);
        Event endOfWork = new Event(true, mContext.getString(R.string.test2_name),
                mContext.getString(R.string.test2_text), date2.getTimeInMillis(), EventStyle.CLOCK);
        cupboard().withDatabase(db).put(endOfWork);
    }

    /**
     * Updates tables and replaces some EventStyle values if they were deleted.
     *
     * @param db         database to update
     * @param oldVersion old version number
     * @param newVersion new version number
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if ((oldVersion == 1) && (newVersion > 1)) {
            replaceEventStyle(db, 2, EventStyle.PINETREE);
            replaceEventStyle(db, 3, EventStyle.BLUE);
            replaceEventStyle(db, 4, EventStyle.CHRISTMAS);
            replaceEventStyle(db, 7, EventStyle.CHRISTMAS);
        }
        if (newVersion == 3) {
            replaceEventStyle(db, 5, EventStyle.GRASS);
            replaceEventStyle(db, 6, EventStyle.FLOWER);
            replaceEventStyle(db, 8, EventStyle.BUTTERFLY);
            replaceEventStyle(db, 9, EventStyle.CHRISTMASFRAME);
            replaceEventStyle(db, 10, EventStyle.CHRISTMASTOWN);
            replaceEventStyle(db, 11, EventStyle.FALLINGHEARTS);
            replaceEventStyle(db, 12, EventStyle.GREEN);
            replaceEventStyle(db, 13, EventStyle.HALLOWEEN);
            replaceEventStyle(db, 14, EventStyle.HEART);
            replaceEventStyle(db, 15, EventStyle.PUMPKIN);
            replaceEventStyle(db, 16, EventStyle.SEA);
            replaceEventStyle(db, 17, EventStyle.VALENTINERED);
            replaceEventStyle(db, 18, EventStyle.PINK);
            replaceEventStyle(db, 19, EventStyle.BIRD);
            replaceEventStyle(db, 20, EventStyle.BLUE);
            replaceEventStyle(db, 21, EventStyle.CLOCK);
            replaceEventStyle(db, 22, EventStyle.FISH);
            replaceEventStyle(db, 23, EventStyle.GLOWINGPINETREE);
            replaceEventStyle(db, 24, EventStyle.IVORY);
            replaceEventStyle(db, 25, EventStyle.LEAF);
            replaceEventStyle(db, 26, EventStyle.PINETREE);
            replaceEventStyle(db, 27, EventStyle.REDSNOW);
        }
        cupboard().withDatabase(db).upgradeTables();
    }

    /**
     * Replaces EventStyle value in all the records. Called if some EventStyle constants was deleted.
     *
     * @param oldStyle deleted EventStyle constant
     * @param newStyle EventStyle constant to replace with
     */
    public void replaceEventStyle(SQLiteDatabase db, int oldStyle, int newStyle) {
        try {
            QueryResultIterable<Event> itr = cupboard().withDatabase(db).query(Event.class)
                    .withSelection("eventStyle = ?", Integer.toString(oldStyle)).query();
            for (Event event : itr) {
                event.setEventStyle(newStyle);
                cupboard().withDatabase(db).update(Event.class, cupboard().withEntity(Event.class).
                        toContentValues(event));
            }
            itr.close();
        } catch (Exception e) {
            Log.e(Constants.LOG_TAG, "Can't read events from database");
        }
    }
}
