package biz.no_ip.palaven.countdown;

import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.List;

/**
 * The configuration screen for the {@link EventWidget EventWidget} AppWidget.
 */
public class EventWidgetConfigureActivity extends Activity {

    private static final String PREFS_NAME = "biz.no_ip.palaven.countdown.EventWidget";
    private static final String PREF_PREFIX_KEY = "appwidget_";
    private static final String PREF_ID = "id";
    int mAppWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
    Spinner mEventSpinner;

    /**
     * Gets selected event and puts it to the preferences. Then creates widget and calls it.
     */
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
            final Context context = EventWidgetConfigureActivity.this;

            // When the button is clicked, store the string locally
            Event widgetEvent = (Event) mEventSpinner.getSelectedItem();
            saveEventPref(context, mAppWidgetId, widgetEvent);

            // It is the responsibility of the configuration activity to update the app widget
            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
            EventWidget.updateAppWidget(context, appWidgetManager, mAppWidgetId);

            // Make sure we pass back the original appWidgetId
            Intent resultValue = new Intent();
            resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, mAppWidgetId);
            setResult(RESULT_OK, resultValue);
            finish();
        }
    };

    public EventWidgetConfigureActivity() {
        super();
    }

    /**
     * Saves event id to the preferences.
     *
     * @param context     context to get the preferences
     * @param appWidgetId widget id
     * @param event       widget's event
     */
    // Write the prefix to the SharedPreferences object for this widget
    static void saveEventPref(Context context, int appWidgetId, Event event) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.putLong(PREF_PREFIX_KEY + appWidgetId + PREF_ID, event.get_id());
        prefs.apply();
    }

    /**
     * Gets event id from the preferences and finds the event.
     *
     * @param context     context to get the preferences
     * @param appWidgetId widget id
     * @return found event
     */
    // Read the prefix from the SharedPreferences object for this widget.
    // If there is no preference saved, get the default from a resource
    static Event loadEventPref(Context context, int appWidgetId) {
        SharedPreferences prefs = context.getSharedPreferences(PREFS_NAME, 0);
        Database db = new Database(context);
        return db.getEvent(prefs.getLong(PREF_PREFIX_KEY + appWidgetId + PREF_ID, 0));
    }

    /**
     * Removes all the preferences for the widget.
     *
     * @param context     context to get the preferences
     * @param appWidgetId widget id
     */
    static void deleteEventPref(Context context, int appWidgetId) {
        SharedPreferences.Editor prefs = context.getSharedPreferences(PREFS_NAME, 0).edit();
        prefs.remove(PREF_PREFIX_KEY + appWidgetId);
        prefs.apply();
    }

    /**
     * Creates view, fills spinner with active events from database
     *
     * @param icicle used in {@Link #Activity.onCreate}
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        // Set the result to CANCELED.  This will cause the widget host to cancel
        // out of the widget placement if the user presses the back button.
        setResult(RESULT_CANCELED);

        setContentView(R.layout.event_widget_configure);
        mEventSpinner = (Spinner) findViewById(R.id.spinner);
        Database db = new Database(getBaseContext());
        List<Event> eventList = db.getActiveEventList();
        Event[] events = new Event[eventList.size()];
        events = eventList.toArray(events);
        EventAdapter adapter = new EventAdapter(getBaseContext(), R.layout.event_text_view, events);
        mEventSpinner.setAdapter(adapter);
        findViewById(R.id.add_button).setOnClickListener(mOnClickListener);

        // Find the widget id from the intent.
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            mAppWidgetId = extras.getInt(
                    AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
        }

        // If this activity was started with an intent without an app widget ID, finish with an error.
        if (mAppWidgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
            finish();
        }
    }

    /**
     * Adapter for events spinner.
     */
    class EventAdapter extends ArrayAdapter {
        private Context context;
        private int textViewResourceId;
        private Event[] objects;

        /**
         * Sets the layout for selected item and events array.
         *
         * @param context            application context
         * @param textViewResourceId TextView layout
         * @param objects            events array
         */
        public EventAdapter(Context context, int textViewResourceId, @NonNull Event[] objects) {
            super(context, textViewResourceId, objects);

            this.context = context;
            this.textViewResourceId = textViewResourceId;
            this.objects = objects;
        }

        /**
         * Gets the event's name and shows it.
         *
         * @param position    current event position
         * @param convertView text view
         * @param parent      not used
         * @return view
         */
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = View.inflate(context, textViewResourceId, null);
            TextView tv = (TextView) convertView;
            tv.setText(objects[position].getName());
            return convertView;
        }

        /**
         * Gets the event's name and show it.
         *
         * @param position    current event position
         * @param convertView text view
         * @param parent      not used
         * @return view
         */
        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            if (convertView == null)
                convertView = View.inflate(context, android.R.layout.simple_spinner_dropdown_item,
                        null);
            TextView tv = (TextView) convertView;
            tv.setText(objects[position].getName());
            return convertView;
        }
    }
}

