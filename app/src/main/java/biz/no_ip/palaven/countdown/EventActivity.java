package biz.no_ip.palaven.countdown;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Switch;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Activity for editing or creating event. Contains input fields and switch. By clicking on the
 * date field, a DatePickerDialog appears. By clicking on the time field, a TimePickerDialog
 * appears. By clicking on the style field, a StylePickerDialog appears. On the Save button event
 * is saved to the database.
 *
 * @author Aliya Gainetdinova
 */
public class EventActivity extends AppCompatActivity
        implements DatePickerDialogFragment.DatePickedListener,
        TimePickerDialogFragment.TimePickedListener,
        StylePickerDialogFragment.StylePickedListener {

    public static final String ARG_EVENT = "event";
    public static final int EDIT_EVENT_REQUEST = 1;
    public static final int ADD_EVENT_REQUEST = 2;

    private Event mEvent;
    private TextView mDateText;
    private TextView mTimeText;
    private TextView mEventStyleText;
    private int mCurrentStyle;
    private Database mDB;

    /**
     * Loads values from assigned event or creates empty event.
     *
     * @param savedInstanceState used in parent's method
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        mDB = new Database(this);

        mEvent = getIntent().getParcelableExtra(ARG_EVENT);
        if (mEvent == null) {
            mEvent = new Event(true, "", "", new Date().getTime(), EventStyle.FLOWER);
        }
        mDateText = (TextView) findViewById(R.id.textDate);
        mTimeText = (TextView) findViewById(R.id.textTime);
        mEventStyleText = (TextView) findViewById(R.id.textEventStyle);

        setValuesToViews();

        ImageButton saveButton = (ImageButton) findViewById(R.id.btnSave);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getValuesFromViews();

                mDB.saveEvent(mEvent);

                Intent intent = new Intent();
                intent.putExtra(ARG_EVENT, mEvent);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        ImageButton cancelButton = (ImageButton) findViewById(R.id.btnCancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(RESULT_CANCELED);
                finish();
            }
        });
    }

    /**
     * Sets Canceled result to activity
     */
    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    /**
     * Shows event's values in input fields.
     */
    private void setValuesToViews() {
        Switch active = (Switch) findViewById(R.id.switchActive);
        active.setChecked(mEvent.getActive());
        EditText name = (EditText) findViewById(R.id.textName);
        name.setText(mEvent.getName());
        EditText text = (EditText) findViewById(R.id.textText);
        text.setText(mEvent.getText());
        showDate(new Date(mEvent.getDateTime()).getTime());
        showTime(new Date(mEvent.getDateTime()).getTime());
        showStyle(mEvent.getEventStyle());
    }

    /**
     * Loads values from input fields to the event object.
     */
    private void getValuesFromViews() {
        Switch active = (Switch) findViewById(R.id.switchActive);
        mEvent.setActive(active.isChecked());
        EditText name = (EditText) findViewById(R.id.textName);
        mEvent.setName(name.getText().toString());
        EditText text = (EditText) findViewById(R.id.textText);
        mEvent.setText(text.getText().toString());
        mEvent.setDateTime(getDateTime().getTimeInMillis());
        mEvent.setEventStyle(mCurrentStyle);
    }

    /**
     * Calls a DatePickerDialog and gives it the date.
     *
     * @param v not used
     */
    public void onDateClick(View v) {
        DialogFragment dateFragment = new DatePickerDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(DatePickerDialogFragment.ARG_DATE, getDateTime());
        dateFragment.setArguments(args);
        dateFragment.show(getSupportFragmentManager(), "datePicker");
    }

    /**
     * Calls a TimePickerDialog and gives it the time.
     *
     * @param v not used
     */
    public void onTimeClick(View v) {
        DialogFragment timeFragment = new TimePickerDialogFragment();
        Bundle args = new Bundle();
        args.putSerializable(TimePickerDialogFragment.ARG_TIME, getDateTime());
        timeFragment.setArguments(args);
        timeFragment.show(getSupportFragmentManager(), "timePicker");
    }

    /**
     * Calls a StylePickerDialog and gives it the EventStyle constant.
     *
     * @param v not used
     */
    public void onStyleClick(View v) {
        DialogFragment styleFragment = new StylePickerDialogFragment();
        Bundle args = new Bundle();
        args.putInt(StylePickerDialogFragment.ARG_STYLE, mCurrentStyle);
        styleFragment.setArguments(args);
        styleFragment.show(getSupportFragmentManager(), "stylePicker");
    }

    /**
     * Calls {@Link #showDate}
     *
     * @param date date to show
     */
    public void onDatePicked(Calendar date) {

        showDate(date.getTimeInMillis());
    }

    /**
     * Calls {@Link #showTime}
     *
     * @param date time to show
     */
    public void onTimePicked(Calendar date) {

        showTime(date.getTimeInMillis());
    }

    /**
     * Calls {@Link #showStyle}
     *
     * @param style style constant to show
     */
    public void onStylePicked(int style) {
        showStyle(style);
    }

    /**
     * Sets date to TextView
     *
     * @param date date to set
     */
    private void showDate(long date) {
        SimpleDateFormat formatterDate = new SimpleDateFormat("dd/MM/yyyy");
        mDateText.setText(formatterDate.format(date));
    }

    /**
     * Sets time to TextView
     *
     * @param time time to set
     */
    private void showTime(long time) {
        SimpleDateFormat formatterTime = new SimpleDateFormat("HH:mm:ss");
        mTimeText.setText(formatterTime.format(time));
    }

    /**
     * Sets style's name to TextView
     *
     * @param style EventStyle constant to get style's name
     */
    private void showStyle(int style) {
        mEventStyleText.setText(EventStyle.getStyle(style));
        mCurrentStyle = style;
    }

    /**
     * Parses datetime from TextViews
     *
     * @return received datetime
     */
    private Calendar getDateTime() {
        GregorianCalendar datetime = new GregorianCalendar();
        String dateStr = mDateText.getText().toString();
        String timeStr = mTimeText.getText().toString();
        datetime.set(Calendar.DATE, Integer.parseInt(dateStr.substring(0, 2)));
        datetime.set(Calendar.MONTH, Integer.parseInt(dateStr.substring(3, 5)) - 1);
        datetime.set(Calendar.YEAR, Integer.parseInt(dateStr.substring(6, 10)));
        int hours = Integer.parseInt(timeStr.substring(0, 2));
        datetime.set(Calendar.HOUR_OF_DAY, hours);
        datetime.set(Calendar.MINUTE, Integer.parseInt(timeStr.substring(3, 5)));
        datetime.set(Calendar.SECOND, Integer.parseInt(timeStr.substring(6, 8)));
        return datetime;
    }
}
