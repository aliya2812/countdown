package biz.no_ip.palaven.countdown;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * The class describes events: they have name ("New Year"), text ("Before the new year less than"),
 * activity (show this event or not), datetime (01.01.2016 00:00:00) and style (one of the
 * EventStyle constants). Also the class serves database table. Implementing the Parcelable
 * interface is needed to transfer event to another activity or fragment.
 *
 * @author Aliya Gainetdinova
 */
public class Event implements Parcelable {

    private Long _id;
    private int active;
    private String name;
    private String text;
    private long dateTime;
    private int eventStyle;

    public Event() {

    }

    public Event(boolean active, String name, String text, long dateTime, int eventStyle) {
        this.active = active ? 1 : 0;
        this.name = name;
        this.text = text;
        this.dateTime = dateTime;
        this.eventStyle = eventStyle;
    }

    public Event(Parcel in) {
        String[] data = new String[6];
        in.readStringArray(data);
        _id = Long.parseLong(data[0]);
        active = Integer.parseInt(data[1]);
        name = data[2];
        text = data[3];
        dateTime = Long.parseLong(data[4]);
        eventStyle = Integer.parseInt(data[5]);
    }

    public boolean getActive() {
        return active == 1;
    }

    public String getName() {
        return name;
    }

    public String getText() {
        return text;
    }

    public long getDateTime() {
        return dateTime;
    }

    public int getEventStyle() {
        return eventStyle;
    }

    public void setActive(boolean mActive) {
        this.active = mActive ? 1 : 0;
    }

    public void setActive(int mActive) {
        this.active = mActive;
    }

    public void setName(String mName) {
        this.name = mName;
    }

    public void setText(String mText) {
        this.text = mText;
    }

    public void setDateTime(long mDateTime) {
        this.dateTime = mDateTime;
    }

    public void setEventStyle(int mEventStyle) {
        this.eventStyle = mEventStyle;
    }

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;
    }

    /**
     * Does nothing.
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeStringArray(new String[]{
                Long.toString(_id),
                Integer.toString(active),
                name,
                text,
                Long.toString(dateTime),
                Integer.toString(eventStyle)});
    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {

        @Override
        public Event createFromParcel(Parcel source) {
            return new Event(source);
        }

        @Override
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };
}
